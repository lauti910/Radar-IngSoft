## Radar Tecnologico ##

|        |Plataformas | Herramientas | Tecnicas | Lenguajes |
|--      |------------|--------------|----------|-----------|
|Uso     | Eclipse, SourceTree, Pycharm  | Maven, Git| TDD | Python, Java |
|Adoptar | Atom | Gradle | Metodologias agiles | Markdown, Javascript |
|Probar  | Android Studio | Trello |  | Rubi, Angular |
|Evaluar |  |  |  | Angular 2, GO|
|Evitar  |  |  |  | Haskell |